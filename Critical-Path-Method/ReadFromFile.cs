﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Critical_Path_Method
{
    class ReadFromFile
    {
        private List<Process> processes = new List<Process>();
        string path;


        public  ReadFromFile(string path)
        {
            if(path != null)
            {
                this.path = path;
                AddProcessToList();
             
            } else
            {
                ReadDefaultFile();
           
            }
        }

        private void ReadDefaultFile()
        {
      
            string dir = Directory.GetCurrentDirectory();
            path = Path.Combine(dir, @"Files\Netzplan.txt");
            path = path.Replace("\\bin\\Debug", "");
            AddProcessToList();
        }

        private void AddProcessToList()
        {
            int counter = 0;
            foreach (string line in File.ReadAllLines(path)) 
            {
                if (counter != 0)
                {
                    string[] parameters = line.Split('\t');
                    int id = Int16.Parse(parameters[0]);
                    string name = parameters[1];
                    int duration = Int16.Parse(parameters[2]);
                    Process process = new Process(id, name, duration);

                    // Add predcessors to Process
                    if (!String.Equals(parameters[3], "-"))
                    {
                        string[] predecessors = parameters[3].Split(';');
                        foreach (var predecessor in predecessors)
                        {
                            int predecessorId = Int16.Parse(predecessor);
                            process.AddPredecessor(predecessorId);
                        }
                    }

                    // Add successors to Process
                    if (!String.Equals(parameters[4], "-"))
                    {
                        string[] successors = parameters[4].Split(';');
                        foreach (var successor in successors)
                        {
                            int successorId = Int16.Parse(successor);
                            process.AddSuccessor(successorId);
                        }
                    }
                    
                    processes.Add(process);


                }
                counter++;
            }
        }



        public List<Process> getProccess()
        {
            return processes;
        }
    }
}
