﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Critical_Path_Method
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                openFileDialog.InitialDirectory = "c:\\";
                openFileDialog.Title = "Select File to Upload";
                openFileDialog.FileName = string.Empty;

                openFileDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                DialogResult _result = openFileDialog.ShowDialog();
            
                if (_result == DialogResult.OK)
                {
                    TextFile.Text = openFileDialog.FileName;
                    ReadFile(openFileDialog.FileName);
                    DisplayCriticalProcces();
                    
                }

            }
            catch (Exception)
            {
                throw;
            }
        }


        private void button1_Click_1(object sender, EventArgs e)
        {
            ReadFile(null);
            DisplayCriticalProcces();
           

        }

        private void ReadFile(string FileName)
        {
            ReadFromFile fileReader = new ReadFromFile(FileName);

            Globals.processes = fileReader.getProccess();
            foreach (Process process in Globals.processes)
            {
                // Calculate FAZ and FEZ
                process.CalcFAZ(); 
                process.CalcFEZ();
            }

            Globals.processes.Reverse();
            foreach (Process process in Globals.processes)
            {
                //Calculate SEZ and SAZ
                process.CalcSEZ(); 
                process.CalcSAZ();
            }

            Globals.duration = Globals.processes[0].GetFAZ();

            foreach (Process process in Globals.processes)
            {
               process.CalcFP();
               process.CalcGP();
               Console.WriteLine("ID: " + process.GetId());
               Console.WriteLine("FAZ: " + process.GetFAZ());
               Console.WriteLine("FEZ: " + process.GetFEZ());
               Console.WriteLine("SAZ: " + process.GetSAZ());
               Console.WriteLine("SEZ: " + process.GetSEZ());
               Console.WriteLine("Freie Puffer: " + process.GetFP());
               Console.WriteLine("Gesamte Puffer: " + process.GetGP());

            }
        }

        public void DisplayCriticalProcces()
        {
            UpLoadFile.Hide();

            List<Process> criticalProcesses = new List<Process>();

            foreach (Process process in Globals.processes)
            {

                if (process.IsCritical())
                {
                    criticalProcesses.Add(process);
                }
            }

            GraphicProcess graphicProcess = new GraphicProcess(criticalProcesses);
            List<Panel> graphicProcesses = graphicProcess.getGraphicProcesses();

            foreach (Panel process in graphicProcesses)
            {
                Controls.Add(process);
            }

            Controls.Add(SetCriticalProcessLabel(graphicProcesses.Count == 0));
            Controls.Add(SetDurationLabel());

            WindowState = FormWindowState.Maximized; // Maximiert Programm Fenster
        }

        public Label SetCriticalProcessLabel(Boolean noProcess)
        {
            Label label = new Label();
            label.Location = new Point(20, 20);
            if (noProcess)
            {
                label.Text = "No critical processes found";
            }
            else
            {
                label.Text = "Critical processes:";
            }
            return label;
        }

        public Label SetDurationLabel()
        {
            Label label = new Label();
            label.Location = new Point(20, 45);
            string duration = Globals.duration.ToString();
            label.Text = "Project duration: " + duration;
            return label;
        }


        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void textBox1_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void UpLoadFile_Enter(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }
    }
}
