﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Critical_Path_Method
{
    class Process
    {
        private int id;
        private string name;
        private int duration;

        private int FAZ = 0;
        private int FEZ = 0;
        private int SAZ = 0;
        private int SEZ = 0;

        private int GP = 0;
        private int FP = 0;

        private List<int> predecessors = new List<int>();
        private List<int> successors = new List<int>();

        public Process(int id, string name, int duration)
        {
            this.id = id;
            this.name = name;
            this.duration = duration;

        }

        public void AddPredecessor(int preProcess)
        {
            predecessors.Add(preProcess);
        }

        public void AddSuccessor(int succProcess)
        {
            successors.Add(succProcess);
        }

        public int GetId()
        {
            return id;
        }

        public string GetName()
        {
            return name;
        }

        public int GetDuration()
        {
            return duration;
        }

        public List<int> GetSuccessors()
        {
            return successors;
        }

        public List<int> GetPredecessor()
        {
            return successors;
        }


        public void SetId(int id)
        {
            this.id = id;
        }

        public void SetName(string name)
        {
            this.name = name;
        }

        public void SetDuration(int duration)
        {
            this.duration = duration;
        }

        public int GetSEZ()
        {
            return SEZ;
        }

        public int GetFEZ()
        {
            return FEZ;
        }

        public int GetFAZ()
        {
            return FAZ;
        }

        public int GetSAZ()
        {
            return SAZ;
        }

        public int GetGP()
        {
            return GP;
        }

        public int GetFP()
        {
            return FP;
        }

        public Boolean IsPredecessor(Process process)
        {
            foreach (int succProcess in successors)
            {
                if (succProcess == process.GetId())
                    return true;
            }
            return false;
        }

        public Boolean IsSuccessors(Process process)
        {
            foreach (int preProcess in predecessors)
            {
                if (preProcess == process.GetId())
                    return true;
            }
            return false;
        }

        public Boolean IsStartProcess()
        {
            if (predecessors.Count == 0)
                return true;
            else return false;
        }

        public Boolean IsEndProcess()
        {
            if (successors.Count == 0)
                return true;
            else return false;
        }

        public void CalcFAZ()
        {
            if (IsStartProcess())
                FAZ = 0;
            else
            {
                List<Process> prodecessorList = new List<Process>();
                
                foreach (int prodecessor in predecessors)
                {
                    prodecessorList.Add(Globals.processes.Find(item => item.GetId() == prodecessor));
                }
              
                for (int i = 0; i < prodecessorList.Count; i++)
                {

                    int prodecessorFEZ = prodecessorList[i].GetFEZ();

                    if (prodecessorFEZ > FAZ)
                    {
                        FAZ = prodecessorFEZ;
                    } 
                }
            }
        }

        public void CalcFEZ()
        {
            FEZ = FAZ + duration;
        }

        public void CalcSEZ()
        {
            if (IsEndProcess())
            {
                foreach(int predecessor in predecessors)
                {
                    Process process = Globals.processes.Find(item => item.GetId() == predecessor);
                    if (process.GetFEZ() > SEZ)
                    {
                        SEZ = process.GetFEZ();
                    }
                   
                }
            } else
            {
                for(int i = 0; i < successors.Count; i++)
                {
                    Process process = Globals.processes.Find(item => item.GetId() == successors[i]);
                    if (i == 0)
                        SEZ = process.GetSAZ();
                    else if (process.GetSAZ() < SEZ)
                        SEZ = process.GetSAZ();
                }
            }
        }

        public void CalcSAZ()
        {
            SAZ = SEZ - duration;
        }

        public Boolean IsCritical()
        {
            if (id != 1 && id != Globals.processes.Count )
            {
                return FAZ == SAZ;
            }
            else return false;
            
        }

        public void CalcGP()
        {
            GP = SEZ - FEZ;
        }

        public void CalcFP()
        {
            int minFAZ = 0;
            List<Process> successorsList = new List<Process>();
            foreach (int successor in successors)
            {
                successorsList.Add(Globals.processes.Find(item => item.GetId() == successor));
            }

                for (int i = 0; i < successorsList.Count; i++)
                {
                  
                    if (i == 0)
                    {
                        minFAZ = successorsList[i].GetFAZ();
                    }
                    else if (successorsList[i].GetFAZ() < minFAZ)
                    { 
                        minFAZ = successorsList[i].GetFAZ();
                    }
                }

            int sum = minFAZ - FEZ;
            if (sum < 0)
            {
                FP = 0;
            } else
                FP = sum;
        }
    }
}
