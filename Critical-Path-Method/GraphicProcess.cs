﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Critical_Path_Method
{
    class GraphicProcess
    {
        private List<Panel> graphicProcesses = new List<Panel>();
        private int x = 20;
        private int y = 60;
     

        public GraphicProcess(List<Process> processes)
        {
           
            for (int i = 0; i < processes.Count; i++)
            {
              
                Panel panel = CreatePanel(processes[i]);
                if( i == 0)
                {
                    panel.Location = new Point(x, y);
                }
                else
                {
                    panel.Location = new Point(x, y + 120);
                }

                graphicProcesses.Add(panel);
            }
        }

        public int nextNode()
        {

            int node = 0;
            return node;
        }

        public Panel CreatePanel(Process process)
        {
           
            Panel panel = new Panel();
            GroupBox gb = new GroupBox();
            TableLayoutPanel table = CreateTable(process);
            Label FAZ = SetLabel(process.GetFAZ().ToString());
            Label FEZ = SetLabel(process.GetFEZ().ToString());
            Label SAZ = SetLabel(process.GetSAZ().ToString());
            Label SEZ = SetLabel(process.GetSEZ().ToString());
            FAZ.Location = new Point(0, 6);
            FEZ.Location = new Point(133, 88);
            SAZ.Location = new Point(0, 88);
            SEZ.Location = new Point(133, 6);
            gb.AutoSize = true;

            panel.MaximumSize = new Size(147, 120);
            panel.AutoSize = true;

            panel.Controls.Add(FAZ);
            panel.Controls.Add(FEZ);
            panel.Controls.Add(SAZ);
            panel.Controls.Add(SEZ);
            panel.Controls.Add(table);
            return panel;
        }

        public TableLayoutPanel CreateTable(Process process)
        {
            TableLayoutPanel table = new TableLayoutPanel();
            
            table.Location = new Point(0, 20);
            table.Size = new Size(145, 70);
            table.BackColor = SystemColors.ActiveCaption;
            table.ColumnCount = 3;
            table.RowCount = 2;
            table.CellBorderStyle = TableLayoutPanelCellBorderStyle.InsetDouble;
            table.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 20));
            table.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 40));
            table.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 40));
            table.RowStyles.Add(new RowStyle(SizeType.Percent, 70));
            table.RowStyles.Add(new RowStyle(SizeType.Percent, 30));

            Label id = new Label();
            id.AutoSize = true;
            id.Text = process.GetId().ToString();
            id.TextAlign = ContentAlignment.MiddleCenter;
            table.Controls.Add(id, 0, 0);

            Label duration = new Label();
            duration.Text = process.GetDuration().ToString();
            duration.TextAlign = ContentAlignment.MiddleCenter;
            table.Controls.Add(duration, 0, 1);

            Label totalBuffer = new Label();
            totalBuffer.Text = process.GetGP().ToString(); // TODO
            totalBuffer.TextAlign = ContentAlignment.MiddleCenter;
            table.Controls.Add(totalBuffer, 1, 1);

            Label freeBuffer = new Label();
            freeBuffer.Text = process.GetFP().ToString(); // TODO
            freeBuffer.TextAlign = ContentAlignment.MiddleCenter;
            table.Controls.Add(freeBuffer, 2, 1);

            Label designation = new Label();
            designation.Size = new Size(110, 50);

            designation.Text = process.GetName();
            designation.TextAlign = ContentAlignment.MiddleCenter;

            table.Controls.Add(designation);
            table.SetCellPosition(designation, new TableLayoutPanelCellPosition(1, 0));
            table.SetColumnSpan(designation, 2);



            return table;
        }

        public Label SetLabel(string text)
        {
            Label label = new Label();
            label.Text = text;
            label.Size = new Size(9, 12);
            return label;
        }

        public List<Panel> getGraphicProcesses()
        {
            return graphicProcesses;
        }
    }
}
