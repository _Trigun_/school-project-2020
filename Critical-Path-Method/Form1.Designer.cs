﻿namespace Critical_Path_Method
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.UpLoadFile = new System.Windows.Forms.GroupBox();
            this.DefaultButton = new System.Windows.Forms.Button();
            this.TextFile = new System.Windows.Forms.TextBox();
            this.button = new System.Windows.Forms.Button();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.backgroundWorker2 = new System.ComponentModel.BackgroundWorker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.UpLoadFile.SuspendLayout();
            this.SuspendLayout();
            // 
            // UpLoadFile
            // 
            this.UpLoadFile.BackColor = System.Drawing.SystemColors.Control;
            this.UpLoadFile.Controls.Add(this.label3);
            this.UpLoadFile.Controls.Add(this.label2);
            this.UpLoadFile.Controls.Add(this.label1);
            this.UpLoadFile.Controls.Add(this.DefaultButton);
            this.UpLoadFile.Controls.Add(this.TextFile);
            this.UpLoadFile.Controls.Add(this.button);
            this.UpLoadFile.ForeColor = System.Drawing.SystemColors.ControlText;
            this.UpLoadFile.Location = new System.Drawing.Point(12, 12);
            this.UpLoadFile.Name = "UpLoadFile";
            this.UpLoadFile.Size = new System.Drawing.Size(324, 170);
            this.UpLoadFile.TabIndex = 0;
            this.UpLoadFile.TabStop = false;
            this.UpLoadFile.Enter += new System.EventHandler(this.UpLoadFile_Enter);
            // 
            // DefaultButton
            // 
            this.DefaultButton.Location = new System.Drawing.Point(9, 106);
            this.DefaultButton.Name = "DefaultButton";
            this.DefaultButton.Size = new System.Drawing.Size(75, 23);
            this.DefaultButton.TabIndex = 1;
            this.DefaultButton.Text = "Default";
            this.DefaultButton.UseVisualStyleBackColor = true;
            this.DefaultButton.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // TextFile
            // 
            this.TextFile.Location = new System.Drawing.Point(51, 35);
            this.TextFile.Name = "TextFile";
            this.TextFile.Size = new System.Drawing.Size(193, 20);
            this.TextFile.TabIndex = 1;
            this.TextFile.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // button
            // 
            this.button.Location = new System.Drawing.Point(9, 32);
            this.button.Name = "button";
            this.button.Size = new System.Drawing.Size(36, 23);
            this.button.TabIndex = 0;
            this.button.Text = "...";
            this.button.UseVisualStyleBackColor = true;
            this.button.Click += new System.EventHandler(this.button1_Click);
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 90);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Use Default File";
            this.label1.Click += new System.EventHandler(this.label1_Click_1);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Upload File:";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(16, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "or";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(343, 186);
            this.Controls.Add(this.UpLoadFile);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "Form1";
            this.Text = "Critical-Path-Method";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.UpLoadFile.ResumeLayout(false);
            this.UpLoadFile.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.TextBox TextFile;
        private System.Windows.Forms.Button button;
        private System.Windows.Forms.Button DefaultButton;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.ComponentModel.BackgroundWorker backgroundWorker2;
        private System.Windows.Forms.GroupBox UpLoadFile;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
    }
}

